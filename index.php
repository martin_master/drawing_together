    <head>
        <title>Raphael Play</title>
		<!--Libs-->
        <script type="text/javascript" src="javascriptlibs/raphael0152C.js"></script>
		<script type="text/javascript" src="javascriptlibs/dump.js"></script>
		<script type="text/javascript" src="javascriptlibs/jquery-1.12.0.min.js"></script>
		<!--Custom-->
        <script type="text/javascript" src="javascript/Drawpad_mobile.js"></script>
        <style type="text/css">
            #canvas_container {
                width: 500px;
                border: 1px solid #aaa;
            }
        </style>
    </head>
    <body>
        <div id="canvas_container"></div>
    </body>
</html>